import React from "react";
import { Row, Col, Modal, ButtonGroup, Button } from "react-bootstrap";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import ModalInput from "../UI/ModalInput/ModalInput";
import { Link } from "react-router-dom";
import { FaFacebookF, GrGooglePlus } from "react-icons/all";

const StudentRegister = (props) => {
  return (
    <Modal
      className="student-modal"
      show={props.show}
      centered
      onHide={() => props.setShow(false)}
    >
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body>
        <div className="d-flex justify-content-between p-3">
          <div className="modal-left-side">
            <div className="Login">
              <div className="heading pb-4">Connexion</div>
              <div>
                <ModalInput placeholder="Adresse mail" type="email" />
              </div>

              <div className="my-4 pb-5">
                <ModalInput placeholder="Mot de passe" type="password" />
              </div>

              <div className="mt-5">
                <div className="modal-button" color="primary">
                  Se connecter
                </div>
                <div className="forgetpassword">
                  <Link to="/">Mot de passe oublié ?</Link>
                </div>
                <div className="faceBookGoogleGroup">
                  <div color="primary" className="w-100 modal-social-buttons">
                    <div className="modal-social-btn modal-facebook-btn">
                      <span>
                        <FaFacebookF className="modal-social-icon" />
                      </span>
                      &nbsp;&nbsp; Facebook
                    </div>
                    <div className="modal-social-btn modal-google-btn">
                      <span>
                        <GrGooglePlus className="modal-social-icon-google mr-2" />
                      </span>
                      Google +
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="modal-right-side">
            <div className="Register">
              <div className="heading pb-4">S’inscrire</div>
              <div>
                <ModalInput placeholder="Nom" type="text" />
              </div>
              <div className="my-4">
                <ModalInput placeholder="Prénom" type="text" />
              </div>
              <div className="my-4">
                <ModalInput placeholder="Adresse mail" type="email" />
              </div>
              <div className="my-4">
                <ModalInput placeholder="Numéro de téléphone" type="text" />
              </div>
              <div className="my-4">
                <ModalInput placeholder="Adresse postale" type="text" />
              </div>
              <div className="modal-button modal-register-btn" color="primary">
                S’inscrire
              </div>
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
};
export default StudentRegister;
