import React, { useState } from "react";
import { Navbar, Nav, Image } from "react-bootstrap";
import { Link } from "react-router-dom";
import StudentLogin from "./studentLoginModal";
import StudentRegister from "./studentRegisterModal";
/**
 * @author
 * @function Header
 **/

const Header = (props) => {
  const [LoginModalshow, LoginModalsetShow] = useState(false);
  const [RegisterModalshow, RegisterModalsetShow] = useState(false);
  const studentLogin = () => {
    LoginModalsetShow(!LoginModalshow);
  };
  const studentRigister = () => {
    RegisterModalsetShow(!RegisterModalshow);
  };
  return (
    <>
      <StudentLogin show={LoginModalshow} setShow={LoginModalsetShow} />
      <StudentRegister
        show={RegisterModalshow}
        setShow={RegisterModalsetShow}
      />

<Navbar collapseOnSelect expand="xl" variant="light" className="homeNavBar">
  <Navbar.Brand href="#home"><Image src="../images/logoWinaA-r.png" alt="logo" className="navbar-image" /></Navbar.Brand>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link className="navbar-left-link" href="#features">Entreprises&nbsp; l&nbsp; Publier une offre</Nav.Link>
      <Nav.Link className="navbar-left-link" href="#pricing">Importer votre CV</Nav.Link>
      
    </Nav>
    <Nav>
      <Nav.Link href="#deets" className="navbar-right-link">Entreprise</Nav.Link>
      <Nav.Link href="#deets" className=" navbar-right-link" onClick={studentLogin}>Étudiant</Nav.Link>
      <Nav.Link href="#deets" className=" navbar-right-link" onClick={studentRigister}>S’inscrire</Nav.Link>
      <Nav.Link href="#deets" className=" navbar-right-link-end">En savoir plus</Nav.Link>
     
    </Nav>
  </Navbar.Collapse>
</Navbar>

      {/* <Navbar collapseOnSelect expand="lg" className="homeNavBar">
        <Navbar.Brand href="#home">
          <Image src="../images/logoWinaA-r.png" alt="logo" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="navbar-left">
            <Nav.Link href="#" className="company">
              Entreprises
            </Nav.Link>
            <Nav.Link href="#" class="Post_an_offer">
              Poster une offre
            </Nav.Link>
            <Nav.Link href="#" className="upload_cv">
              Importer votre CV
            </Nav.Link>
          </Nav>
          <Nav className="navbar-center">
            <Nav.Link href="#" className="btn btn-primary">
              Entreprise
            </Nav.Link>
            <Nav.Link
              href="#"
              className="btn btn-primary"
              onClick={studentLogin}
            >
              Étudiant
            </Nav.Link>
            <Nav.Link
              href="#"
              className="btn btn-primary"
              onClick={studentRigister}
            >
              S’inscrire
            </Nav.Link>
            <Nav.Link href="#" className="btn btn-default">
              En savoir plus
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar> */}
    </>
  );
};

export default Header;
